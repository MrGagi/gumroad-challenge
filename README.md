# Gumroad Challenge

This is repository which contains code for [Challenge](https://gumroad.notion.site/Coding-challenge-f7aa85150edd41eeb3537aae4632619f).

- **[Frontend application](https://gumroad-challenge-six.vercel.app)** 

- **[API application](https://gumroad-challenge.dragan-jovanovic.com)**

- **[API Docs](https://gumroad-challenge.dragan-jovanovic.com/api-docs)**


# Backend

Backend is running on NodeJS with Socket.io for sockets.

Stuff I would do differently:

- Socket.io is part of the BE application, but otherwise I would make it separate module which can be scaled separetly. Both application should be separeted and scaled like that.

- I would organize them little bit differently, probably group everything by modules **product** and **review**. Then both those modules would contain their routes, repositories, models. And each one would export their own router which is included in base application.


# Frontend

MVP has a git tag **mvp** [MVP](https://bitbucket.org/MrGagi/gumroad-challenge/src/mvp/)

Stuff I would do differently:

- I would probably use axios on React app and defined baseUrl on axios instance, instead of getting it from config. It would still be in .env, just axios would append baseUrl. And it would be wrapped as adapter, so that I don't depend on axios all over the code.

- I would write a way to map data from backend to what my frontend app expects. Because currently, my frontend is kinda depended on the model I have on backend. This should be decopuled.

- Regarding tests, I wanted to write cypress tests which would tests user stories.

- I would create Modal as shared component and use portal to display it. It would be composable from smaller components.