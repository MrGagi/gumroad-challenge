# Gumroad Challenge - Backend

## Start
```
yarn
cp .env.example .env
docker-compose up
```

## Docs
To generate API documentation run:
```
yarn generate-docs
```

It will generate required json file inside docs file, it's generated based on annotations. When application is running you can check docs on url:
```
http://localhost:3000/api-docs
```

## Migrations
To create and migrate database run inside App container:
```
docker ps - Get ID of App Container
docker-compose exec -it {app_container_id} yarn migrate
```