const swaggerJsdoc = require('swagger-jsdoc');
const fs = require('fs');

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Reviews',
      version: '1.0.0',
    },
    servers: [{ url: 'http://localhost:3000/api/v1' }]
  },
  apis: ['./src/routes/**/*Router.js'],
};

const specification = swaggerJsdoc(options);

fs.writeFileSync('./docs/swagger.json', JSON.stringify(specification), "utf-8")