const { APIException } = require("./APIException");

module.exports.NotFoundException = class NotFoundException extends APIException {
  status = 404
  message = "Not found"
}