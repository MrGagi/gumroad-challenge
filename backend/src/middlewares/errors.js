const httpStatus = require('http-status');
const { ValidationError } = require('express-validation');
const config = require('../config');
const { APIException } = require('../exceptions/APIException');

const handler = (err, req, res, next) => {
  const response = {
    code: err.status,
    message: err.message || err.status,
    errors: err.errors,
    stack: err.stack,
  };

  if (!config.isDev) {
    delete response.stack;
  }

  res.status(err.status);
  res.json(response);
};

exports.handler = handler;

exports.errorHandler = (err, req, res, next) => {
  if (err instanceof ValidationError) {
    return handler({
      message: 'Validation Error',
      errors: err.details,
      status: 400,
      stack: err.stack,
    }, req, res, next);
  }

  if (err instanceof APIException) {
    return handler({
      message: err.message,
      status: err.status,
      stack: err.stack,
    }, req, res, next);
  }

  return next();
};

exports.notFound = (req, res, next) => {
  return handler(
    {
      message: 'Not found',
      status: httpStatus.NOT_FOUND,
    },
    req,
    res
  );
};
