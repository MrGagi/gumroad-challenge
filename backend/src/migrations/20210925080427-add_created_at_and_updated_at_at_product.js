'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn("Products", "createdAt", Sequelize.DataTypes.DATE, {
      allowNull: false,
      defaultValue: Sequelize.NOW
    });

    await queryInterface.addColumn("Products", "updatedAt", Sequelize.DataTypes.DATE, {
      allowNull: false,
      defaultValue: Sequelize.NOW
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn("Products", "createdAt");
    await queryInterface.removeColumn("Products", "updatedAt");
  }
};
