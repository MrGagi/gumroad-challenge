const { DataTypes, define } = require('sequelize');
const database = require('../orm/db');
const Review = require('./Review');

const Product = database.define('Products', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: DataTypes.STRING(255),
    allowNull: false,
  }
}, {
  sequelize: database,
  tableName: 'Products'
});

Product.hasMany(Review, { foreignKey: "productId" })

module.exports = Product