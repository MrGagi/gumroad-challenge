const { DataTypes, define } = require('sequelize');
const database = require('../orm/db');

const Review = database.define('Reviews', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  comment: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  rating: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  productId: {
    type: DataTypes.INTEGER,
    references: {
      model: 'Product',
      key: 'id'
    }
  }
}, {
  sequelize: database,
  tableName: 'Reviews'
});

module.exports = Review