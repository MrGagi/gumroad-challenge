const { Sequelize } = require('sequelize');
const config = require('../config')

const sequelize = new Sequelize(`mysql://${config.DB_USER}:${config.DB_PASS}@${config.DB_HOST}:${config.DB_PORT}/${config.DB_NAME}`)

module.exports = sequelize