const Product = require('../models/Product');
const Review = require('../models/Review');

module.exports = class ProductRepository {
  all() {
    return Product.findAll({ include: [ { model: Review }] });
  }

  find(productId) {
    return Product.findByPk(productId);
  }

  async exists(productId) {
    return await this.find(productId) !== null;
  }

  async create(name) {
    return await Product.create({ name });
  }
}