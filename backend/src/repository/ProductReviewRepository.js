const Review = require('../models/Review');

module.exports = class ProductReviewRepository {
  create(productId, comment, rating) {
    return Review.create({ comment, rating, productId });
  }
}