const { Router } = require('express');
const productRouter = require('./product/productRouter');
const productReviewRouter = require('./review/reviewRouter');

const router = Router();
router.use('/product', productRouter);
router.use('/product', productReviewRouter);

module.exports = router;
