const express = require('express');
const ProductRepository = require('../../repository/ProductRepository');
const { createProductRequest } = require('./requests/createProductRequest')
const router = express.Router();

const DEFAULT_PAGINATION_SIZE = 10

/**
 * @openapi
 * /product:
 *   get:
 *     description: Get list of products
 *     responses:
 *       200:
 *         description: Returns list of products
 */
router.get('/', async (req, res, next) => {
  products = await new ProductRepository().all()
  res.send(products);
});

/**
 * @openapi
 * /product:
 *   post:
 *     description: Create product
 *     requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                 type: string
 *     responses:
 *       200:
 *         description: Product is created and stored in database
 */
router.post('/', createProductRequest, async (req, res, next) => {
  newProduct = await new ProductRepository().create(req.body.name)
  res.send(newProduct);
});

module.exports = router;
