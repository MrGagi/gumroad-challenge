const { validate, Joi } = require('express-validation');

module.exports.createProductRequest = validate({
  body: Joi.object({
    name: Joi.string().required().max(255)
  }),
}, {}, { stripUnknown: true });
