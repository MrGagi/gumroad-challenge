const { validate, Joi } = require('express-validation');

module.exports.createProductReviewValidator = validate({
  body: Joi.object({
    comment: Joi.string().required().max(1000),
    rating: Joi.number().required().min(0.5).max(5),
  })
}, {}, { stripUnknown: true });
