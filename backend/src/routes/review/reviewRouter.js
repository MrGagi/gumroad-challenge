const express = require('express');
const ProductRepository = require('../../repository/ProductRepository');
const ProductReviewRepository = require('../../repository/ProductReviewRepository');
const { NotFoundException } = require('../../exceptions/NotFoundException')
const router = express.Router();
const { createProductReviewValidator } = require('./requests/createProductReviewRequest');
const { CHANNEL_REVIEW_CREATED } = require('../../sockets/socket.server');

/**
 * @openapi
 * /product/{productId}/review:
 *   post:
 *     description: Create product
 *     parameters:
 *      - in: path
 *        name: productId
 *        schema:
 *          type: integer
 *        required: true
 *     requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              comment:
 *                 type: string
 *              rating:
 *                 type: number
 *                 min: 0.5
 *                 max: 5
 *     responses:
 *       200:
 *         description: Product is created and stored in database
 */
router.post('/:id/review', createProductReviewValidator, async (req, res, next) => {
  const { comment, rating } = req.body;
  const repository = new ProductReviewRepository();
  const productRepository = new ProductRepository();

  if (!await productRepository.exists(Number(req.params.id))) {
    return next(new NotFoundException())
  }

  newReview = await repository.create(Number(req.params.id), comment, Number(rating))
  req.sockets.send(CHANNEL_REVIEW_CREATED, newReview)
  res.send(newReview);
});

module.exports = router;
