const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const { app } = require('./app');

const port = process.env.PORT || 3000;

const spawnMasterProcesses = () => Array.from(Array(numCPUs)).map(cluster.fork);

const spawnChildProcesses = () => {
  app.listen(port, () => console.log(`Listens on port ${port}`));
};

if (cluster.isMaster) {
  spawnMasterProcesses();
} else {
  spawnChildProcesses();
}

cluster.on('exit', () => cluster.fork());
