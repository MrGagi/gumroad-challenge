const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const swaggerUI = require('swagger-ui-express');
const cors = require('cors')
const http = require('http');

const swaggerDocument = require('../../docs/swagger.json');
const errors = require('../middlewares/errors');
const routes = require('../routes/index');
const { createSocketServer } = require('../sockets/socket.server');
const app = express();

const server = http.createServer(app);
const sockets = createSocketServer(server)

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  req.sockets = sockets;
  next();
})

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use('/api/v1', routes);

app.use(errors.errorHandler);
app.use(errors.notFound);

module.exports.app = app;
module.exports.server = server;
