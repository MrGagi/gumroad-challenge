
const { Server } = require("socket.io");
const http = require('http');

class SocketServer {
  constructor(server) {
    this.server = server;
    this.server.on('connection', (socket) => {
      console.log('User connected to the sockets');
    });
  }

  send(channel, data) {
    this.server.emit(channel, data);
  }
}

module.exports.createSocketServer = (server) => {
  const io = new Server(server, { cors: '*' });

  return new SocketServer(io);
}

module.exports.CHANNEL_REVIEW_CREATED = "REVIEW_CREATED"
