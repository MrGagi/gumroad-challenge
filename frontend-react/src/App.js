import './App.css';
import { ProductList } from './components/product/ProductList';
import { ReviewModal } from './components/review/modals/ReviewModal';
import { SocketsProvider } from './sockets/useSockets';
import { ProductContext, useProducts } from './state/product';
import { ReviewModalContext, useReviewModal } from './state/review-modal';

function App() {
  const productsReducer = useProducts();
  const reviewModalReducer = useReviewModal();

  return <ProductContext.Provider value={productsReducer}>
    <ReviewModalContext.Provider value={reviewModalReducer}>
      <SocketsProvider>
        <ProductList/>
        <ReviewModal/>
      </SocketsProvider>
    </ReviewModalContext.Provider>
  </ProductContext.Provider>
}

export default App;
