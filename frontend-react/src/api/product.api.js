import { config } from '../config';

export async function fetchProducts() {
  const url = config.getUrl(config.endpoints.PRODUCT);
  const response = await fetch(url)
  return await response.json();
}
