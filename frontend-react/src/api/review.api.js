import { config } from '../config';

export async function submitReview(productId, rating, comment) {
  const baseUrl = config.getUrl(
    config.endpoints.PRODUCT_REVIEW.replace(
      ":id",
      productId,
    )
  );

  const response = await fetch(baseUrl, {
    method: "POST",
    body: JSON.stringify({ comment, rating }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  return await response.json();
}
