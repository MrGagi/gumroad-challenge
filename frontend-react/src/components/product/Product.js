import React, { useMemo, useContext } from 'react';
import PropTypes from 'prop-types';
import { Review } from '../review/Review';
import { ReviewStars } from '../review/ReviewStars';
import { ReviewModalContext } from '../../state/review-modal';

const toDecimalPointFive = (number) => {
  return Number((Math.round(number * 2) / 2).toFixed(1))
}

const Product = ({ product }) => {
  const { openModal } = useContext(ReviewModalContext);
  const numberOfReviews = product.Reviews.length;

  const averageRating = useMemo(() => {
    if (numberOfReviews > 0) {
      return toDecimalPointFive(product.Reviews.reduce((sum, review) => {
        return sum + review.rating;
      }, 0) / numberOfReviews)
    }
    return 0;
  }, [product.Reviews, numberOfReviews]);

  const renderReviews = () => {
    if (numberOfReviews === 0) {
      return null;
    }

    return product.Reviews.map((review) => <Review review={review} key={review.id}/>)
  }

  return <div>
    <div>
      <h1 className="book-name" id="book-name">{product.name}</h1>
      <div className="u-row u-space-between u-vertical-center">
        <div className="u-row u-vertical-center">
          <span id="average-rating" className="average-rating-text" alt="Average rating">{averageRating}</span>
          <div id="stars-average-rating">
            <ReviewStars numberOfStars={averageRating} />
          </div>
        </div>

        <button alt="Add new review" className="button button-primary" onClick={() => openModal(product.id)}>
          Add review
        </button>
      </div>
    </div>

    <hr className="separator"/>

    {renderReviews()}
  </div>
}


Product.propTypes = {
  product: PropTypes.object
}

export default Product;
