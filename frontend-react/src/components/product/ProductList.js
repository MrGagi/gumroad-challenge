import React, { useContext, useEffect } from 'react';
import { ProductContext } from '../../state/product';
import Product from './Product';

export function ProductList() {
  const { state, actions: { loadProducts } } = useContext(ProductContext);

  useEffect(() => {
    loadProducts();
  }, []);

  return state.products.map((product) => <Product key={product.id} product={product}/>)
}