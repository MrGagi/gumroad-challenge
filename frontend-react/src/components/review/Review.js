import React from 'react';
import PropTypes from 'prop-types';
import { ReviewStars } from "./ReviewStars";

export const Review = ({ review }) => {
  return <div className="review">
    <div className="u-row u-vertical-center">
      <div>
        <ReviewStars numberOfStars={review.rating} />
      </div>
      <div className="u-margin-md u-row">
        <span className="rating-text" alt="Review rating">{review.rating}</span>
        <span alt="Review comment" className="u-text-secondary">, {review.comment}</span>
      </div>
    </div>
  </div>
}

Review.propTypes = {
  review: PropTypes.object,
}