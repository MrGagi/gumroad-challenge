import React from 'react';
import PropTypes from 'prop-types';

export const ReviewStars = ({ numberOfStars, selectRating }) => {
  function renderStars() {
    const stars = []
    for (let i = 0; i < 5; i++) {
      const isOnlyHalfStar = numberOfStars % 1 !== 0 && i === Math.floor(numberOfStars)
      const isOn = i < numberOfStars;

      stars.push(
        <div key={i} className  ="star-container" onClick={(event) => selectRating(event, i + 1)}>
          <div className={`star ${isOn && !isOnlyHalfStar ? 'on' : ''}`}></div>
          {isOnlyHalfStar && <div className="star half-on"></div>}
        </div>
      );
    }

    return stars;
  }

  return <div className="u-row">{renderStars()}</div>;
}

ReviewStars.propTypes = {
  numberOfStars: PropTypes.number,
  selectRating: PropTypes.func,
}