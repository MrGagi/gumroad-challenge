import React, { useContext, useState } from "react"
import { submitReview } from "../../../api/review.api";
import { ReviewModalContext } from '../../../state/review-modal'
import { ReviewStars } from "../ReviewStars";

export function ReviewModal() {
  const [comment, setComment] = useState("");
  const [rating, setRating] = useState(0.5);
  const { state, closeModal } = useContext(ReviewModalContext)

  const storeReview = async (event) => {
    event.preventDefault();
    const productId = state.selectedProductId;
    await submitReview(productId, rating, comment)
    closeModal();
    resetModal();
  }

  const resetModal = () => {
    setComment("")
    setRating(1)
  }

  const onChangeComment = (event) => {
    setComment(event.target.value);
  }

  const selectRating = (event, newRating) => {
    const isHalfRating = event.nativeEvent.offsetX < event.target.clientWidth / 2;
    setRating(isHalfRating ? newRating - 0.5 : newRating)
  }

  return <div>
    <div className={`modal ${state.visible ? '' : 'hide'}`} id="add-review-modal">
      <div className="u-row u-space-between modal-header">
        <p className="modal-title" alt="Modal title">What's your rating?</p>
        <button alt="Close Add Review modal" className="close-button" onClick={closeModal}>X</button>
      </div>
      <form onSubmit={storeReview}>
        <div>
          <p className="u-margin-top-lg">Rating</p>
          <div id="stars-rating" className="u-margin-top-lg stars-rating">
            <ReviewStars numberOfStars={rating} selectRating={selectRating}/>
          </div>
          <p className="u-margin-top-lg">Review</p>
          <textarea value={comment} onChange={onChangeComment} required className="u-margin-top-lg" alt="Review input" id="review-comment" placeholder="Start typing..."></textarea>
        </div>
        <div className="modal-actions">
          <button alt="Submit button" className="button button-primary" type="submit">
            Submit review
          </button>
        </div>
      </form>
    </div>

    <div id="modal-overlay" className={`overlay ${state.visible ? '' : 'hide'}`}></div>
  </div>
}