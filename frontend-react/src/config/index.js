class Config {
  constructor() {
    this.endpoints = {
      PRODUCT: "/product",
      PRODUCT_REVIEW: "/product/:id/review"
    }

    this.socketUrl = process.env.REACT_APP_SOCKET_URL;

    this.baseUrl = process.env.REACT_APP_BASE_URL;
  }

  getUrl(path) {
    return `${this.baseUrl}${path}`
  }
}

export const config = new Config()
