import { useContext, useMemo, useEffect } from 'react';
import io from 'socket.io-client';
import { config } from '../config';
import { ProductContext } from '../state/product'

export function SocketsProvider({ children }) {
  const { actions: { addReview }} = useContext(ProductContext);

  const LISTENERS = useMemo(() => [
    {
      channel: "REVIEW_CREATED",
      action: addReview
    }
  ], [])

  useEffect(() => {
    const connection = io(config.socketUrl);

    connection.on("connect", () => {
      LISTENERS.forEach(listener => {
        connection.on(listener.channel, listener.action);
      })
    });
  }, [LISTENERS])

  return children;
}