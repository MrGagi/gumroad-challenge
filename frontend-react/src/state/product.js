import React, { useReducer } from 'react';
import { fetchProducts } from '../api/product.api';

export const ProductContext = React.createContext();

const ADD_PRODUCTS_ACTION = 'ADD_PRODUCTS';
const ADD_REVIEW_ACTION = 'ADD_REVIEW';

function productReducer(state, action) {
  switch (action.type) {
    case ADD_PRODUCTS_ACTION:
      return {products: [...state.products, ...action.payload]};
    case ADD_REVIEW_ACTION:
      return {
        products: state.products.map((product) => {
          if (product.id === action.payload.productId) {
            return { ...product, Reviews: [...product.Reviews, action.payload] }
          }

          return product;
        })
      }
    default:
      throw new Error("Unknown action");
  }
}

const initialState = {
  products: []
}

export function useProducts() {
  const [state, dispatch] = useReducer(productReducer, initialState)

  const addProducts = (products) => dispatch({ type: ADD_PRODUCTS_ACTION, payload: products });

  const addReview = (review) => dispatch({ type: ADD_REVIEW_ACTION, payload: review });

  const loadProducts = async () => {
    const products = await fetchProducts()
    addProducts(products);
  }

  return {
    state,
    actions: {
      addProducts,
      loadProducts,
      addReview
    }
  }
}