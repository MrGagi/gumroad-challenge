import React, { useReducer } from "react";

export const ReviewModalContext = React.createContext();

const SHOW_MODAL = 'SHOW_MODAL';
const HIDE_MODAL = 'HIDE_MODAL';

function reviewModalReducer(state, action) {
  switch (action.type) {
    case SHOW_MODAL:
      return { visible: true, selectedProductId: action.selectedProductId };
    case HIDE_MODAL:
      return { visible: false };
    default:
      throw new Error("Unknown action");
  }
}

const initialState = {
  visible: false,
  selectedProductId: null,
}

export function useReviewModal() {
  const [state, dispatch] = useReducer(reviewModalReducer, initialState)

  const openModal = (selectedProductId) => dispatch({ type: SHOW_MODAL, selectedProductId });

  const closeModal = () => dispatch({ type: HIDE_MODAL });

  return {
    state,
    openModal,
    closeModal
  }
}